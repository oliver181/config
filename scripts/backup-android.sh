#!/usr/bin/bash
set -euo pipefail

DATE=`date +%d-%m-%y`
FILE=backup-android-all-shared-apk-$DATE.ab

adb backup -all -shared -apk -f $FILE

par2 create $FILE
