#!/usr/bin/bash
set -euo pipefail

DATE=`date +%d-%m-%Y`
FILE=backup-pc-all-$DATE.tar.xz

pushd /
tar --file=- \
    --create \
    --preserve-permissions \
    --one-file-system \
    --use-compress-program=pixz \
    --exclude=$FILE \
    --exclude=proc \
    --exclude=tmp \
    --exclude=mnt \
    --exclude=dev \
    --exclude=sys \
    --exclude=run \
    --exclude=media \
    --exclude=home/oliver/Backup-alter-PC-2020-07-19.zip \
    --exclude=var/log \
    --exclude=var/cache/apt/archives \
    --exclude=usr/src/linux-headers* \
    --exclude=home/*/.gvfs \
    --exclude=home/*/.cache \
    --exclude=home/*/.local/share/Steam \
    --exclude=home/*/.local/share/Trash \
    --exclude=home/*/.local/share/wineprefix \
    * | pv > $FILE


par2 create $FILE

popd
