#!/usr/bin/bash

export WINEPREFIX=~/.local/share/wineprefix/fusion360-2
mkdir -p $WINEPREFIX
cd $WINEPREFIX
curl https://dl.appstreaming.autodesk.com/production/installers/Fusion%20360%20Client%20Downloader.exe -o ~/Downloads/FusionClientDownloader.exe

winetricks -q vcrun2017 corefonts wininet winhttp atmlib gdiplus iertutil msxml3 msxml6 urlmon win7 dxvk
wine ~/Downloads/FusionClientDownloader.exe
