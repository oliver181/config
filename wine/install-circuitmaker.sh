#!/usr/bin/bash

export WINEPREFIX=~/.local/share/wineprefix/circuitmaker2
mkdir -p $WINEPREFIX
cd $WINEPREFIX

curl https://s3.amazonaws.com/altium-install/CircuitMaker/CircuitMakerSetup.exe -o ~/Downloads/CircuitMakerSetup.exe

winetricks -q gdiplus corefonts riched20 mdac28 msxml6 dotnet40
curl http://web.archive.org/web/20160129053851/http://download.microsoft.com/download/E/6/A/E6A04295-D2A8-40D0-A0C5-241BFECD095E/W2KSP4_EN.EXE -o ~/.cache/winetricks/win2ksp4/W2KSP4_EN.EXE
winetricks -q gdiplus corefonts riched20 mdac28 msxml6 dotnet40
winetricks vd=1920x1080
sync
sleep 5
wine ~/Downloads/CircuitMakerSetup.exe
